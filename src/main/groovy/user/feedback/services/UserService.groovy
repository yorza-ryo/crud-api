package user.feedback.services

import grails.gorm.transactions.Transactional
import groovy.util.logging.Slf4j
import user.feedback.controllers.UserController.UpdateUserRequest
import user.feedback.models.User
import io.reactivex.Single

import javax.annotation.Nullable
import jakarta.inject.Singleton

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Null

@Singleton
@Transactional
@Slf4j
class UserService {

    Single<List<User>> getUsers(){
        return Single.just(User.findAll())
    }

    Single<List<User>> showUser(email){
        def user = User.where{email == email}
        return Single.just(user.find())
    }

    Single<Boolean> deleteUser(id){
        def user = User.get(id)
        user.delete()
    }

    Single<User> assignUser(@NotNull version, @NotNull name, @NotNull email){
        def user = new User()
        user.version = version
        user.name = name
        user.email = email

        Single.just(user.save())
    }

    Single<List> updateUser(@NotNull UpdateUserRequest request, id){
        def user = User.get(id)
        if(user){
            user.name = request.name
            user.email = request.email
            user.version = request.version
        }
        Single.just(user.save() != null)
        return Single.just(user)
    }
}
