package user.feedback.models

import grails.gorm.annotation.Entity
import org.grails.datastore.gorm.GormEntity

@Entity
class User implements GormEntity<User>{
    String name, email

    static mapping = {
        table 'users'
    }
}
