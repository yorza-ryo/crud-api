package user.feedback.controllers

import com.sun.istack.NotNull
import groovy.transform.CompileStatic
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.annotation.QueryValue
import io.reactivex.Single
import user.feedback.models.User
import io.micronaut.http.annotation.PathVariable
import user.feedback.services.UserService
import javax.validation.Valid

@CompileStatic
@Controller('/users')
class UserController {
    UserService userService

    UserController(UserService userService){
        this.userService = userService
    }

    @Get("/getUsers")
    Single<List<User>> getUser(){
        userService.getUsers()
    }

    @Get("/")
    Single<List<User>> getUser(@QueryValue("email") String email){
        userService.showUser(email)
    }

    @Get("/user/{emails}")
    Single<List<User>> showUser(String emails){
        userService.showUser(emails)
    }

    @Get("/delete/{id}")
    deleteUser(Integer id){
        userService.deleteUser(id)
        return [
            'success': true
        ]
    }

    @Post("/")
    assignUser(@NotNull Integer version, @NotNull String name, @NotNull String email){
        userService.assignUser(version, name, email)
        return [
            'success': true
        ]
    }

    @Put("/{id}")
    Single<List> updateUser(@Valid @Body UpdateUserRequest request, @PathVariable String id){
        userService.updateUser(request, id)
    }

    static class UpdateUserRequest {
        @NotNull
        Integer version
        @NotNull
        String name
        @NotNull
        String email
    }

    @Get("/{param}")
    def index(@PathVariable String param) {
        param
    }
}
